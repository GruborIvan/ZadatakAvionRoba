package zadaci;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import model.Avion;
import model.Roba;

public class Z1KreiranjeTabela {
    public static void main(String[] args) {
        ConnectionSource cs = null;
        try
        {
            cs = new JdbcConnectionSource("jdbc:sqlite:avionRoba.db");
            Dao<Avion,Integer> AvionDao = DaoManager.createDao(cs, Avion.class);
            Dao<Roba,Integer> RobaDao = DaoManager.createDao(cs,Roba.class);

            // Brisanje tabela...
            TableUtils.dropTable(cs,Roba.class,true);
            TableUtils.dropTable(cs,Avion.class,true);


            TableUtils.createTable(cs,Avion.class);
            TableUtils.createTable(cs,Roba.class);

        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        finally {
            if (cs != null)
                try
                {
                    cs.close();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
        }
    }
}
