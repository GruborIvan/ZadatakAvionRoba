package zadaci;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import jdk.swing.interop.SwingInterOpUtils;
import model.Avion;
import model.Roba;

import java.util.List;

public class Z3IzmenaVrednosti {
    public static void main(String[] args) {
        ConnectionSource cs = null;
        try
        {
            cs = new JdbcConnectionSource("jdbc:sqlite:avionRoba.db");

            Dao<Avion,Integer> AvionDao = DaoManager.createDao(cs, Avion.class);
            Dao<Roba,Integer> RobaDao = DaoManager.createDao(cs,Roba.class);

            System.out.println("INICIJALNA BAZA");
            List<Roba> Svi = RobaDao.queryForAll();
            for (Roba RR : Svi) {
                System.out.println(RR.toString());
            }

            List<Roba> Izmenjen = RobaDao.queryForEq(Roba.POLJE_OPIS,"Plasticna stolica");
            for (Roba R:Izmenjen) {
                R.setOpis("Drvena stolica");
                RobaDao.update(R);
                break;
            }

            System.out.println("Izmenjena BAZA!!!");
            Svi = RobaDao.queryForAll();
            for (Roba RR : Svi) {
                System.out.println( RR.toString() );
            }

        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        finally {
            if (cs != null)
                try
                {
                    cs.close();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
        }
    }
}
