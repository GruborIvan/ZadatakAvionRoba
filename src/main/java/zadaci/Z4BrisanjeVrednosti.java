package zadaci;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import model.Avion;
import model.Roba;

import java.util.List;

public class Z4BrisanjeVrednosti {
    public static void main(String[] args) {
        ConnectionSource cs = null;
        try
        {
            cs = new JdbcConnectionSource("jdbc:sqlite:avionRoba.db");

            Dao<Avion,Integer> AvionDao = DaoManager.createDao(cs, Avion.class);
            Dao<Roba,Integer> RobaDao = DaoManager.createDao(cs,Roba.class);

            List<Roba> robe = RobaDao.queryForAll();
            for (Roba R:robe) {
                System.out.println(R.toString());
            }

            robe = RobaDao.queryForEq(Roba.POLJE_NAZIV,"Voda");
            for (Roba R:robe) {
                RobaDao.delete(R);
                break;
            }

            robe = RobaDao.queryForAll();
            for (Roba R:robe) {
                System.out.println(R.toString());
            }

        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        finally {
            if (cs != null)
                try
                {
                    cs.close();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
        }
    }
}
