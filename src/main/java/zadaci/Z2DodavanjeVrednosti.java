package zadaci;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import model.Avion;
import model.Roba;

import java.util.List;

public class Z2DodavanjeVrednosti {
    public static void main(String[] args) {
        ConnectionSource cs = null;
        try
        {
            cs = new JdbcConnectionSource("jdbc:sqlite:avionRoba.db");

            Dao<Avion,Integer> AvionDao = DaoManager.createDao(cs, Avion.class);
            Dao<Roba,Integer> RobaDao = DaoManager.createDao(cs,Roba.class);

            Avion A1 = new Avion("Avion1",34);
            Avion A2 = new Avion("Avion2",21);
            AvionDao.create(A1);
            AvionDao.create(A2);


            Roba R1 = new Roba("Patike","Duboke patike",1);
            R1.setA(A1);
            Roba R2 = new Roba("Kosulja","Na duge rukave",0.4);
            R2.setA(A1);
            Roba R3 = new Roba("Voda","Voda za pice",1.4);
            R3.setA(A1);
            Roba R4 = new Roba("Ploce","Drevne ploce",3.4);
            R4.setA(A2);
            Roba R5 = new Roba("Stolica","Plasticna stolica",2.4);
            R5.setA(A2);
            RobaDao.create(R1);
            RobaDao.create(R2);
            RobaDao.create(R3);
            RobaDao.create(R4);
            RobaDao.create(R5);

            List<Avion> lista = AvionDao.queryForAll();
            for (Avion A : lista) {
                System.out.println(A.toString());
            }

            List<Roba> robica = RobaDao.queryForAll();
            for (Roba R : robica) {
                System.out.println(R.toString());
            }

        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        finally {
            if (cs != null)
                try
                {
                    cs.close();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
        }
    }
}
