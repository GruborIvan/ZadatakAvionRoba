package model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "roba")
public class Roba {

    public static final String  POLJE_OPIS = "opis";
    public static final String  POLJE_TEZINA = "tezina";
    public static final String  POLJE_NAZIV = "naziv";

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(columnName = POLJE_OPIS,canBeNull = false)
    private String opis;

    @DatabaseField(columnName = POLJE_NAZIV,canBeNull = false)
    private String naziv;

    @DatabaseField(columnName = POLJE_TEZINA,canBeNull = false)
    private double tezina;

    @DatabaseField(foreign = true,foreignAutoRefresh = true)
    private Avion A;

    public Roba()
    {

    }
    public Roba(String naziv,String opis,double tezina)
    {
        this.opis = opis;
        this.naziv = naziv;
        this.tezina = tezina;

    }

    @Override
    public String toString() {return "ID: " + id + " Opis: " + opis + " Naziv: " + naziv + " Tezina: " + tezina;}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public double getTezina() {
        return tezina;
    }

    public void setTezina(double tezina) {
        this.tezina = tezina;
    }

    public Avion getA() {
        return A;
    }

    public void setA(Avion a) {
        A = a;
    }
}
