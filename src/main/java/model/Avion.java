package model;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "avion")
public class Avion {

    public static final String OPIS_OZNAKA = "oznaka";
    public static final String OPIS_RASPON = "raspon_krila";

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(columnName = OPIS_OZNAKA,canBeNull = false)
    private String opis;

    @DatabaseField(columnName = OPIS_RASPON,canBeNull = false)
    private int raspon_krila;

    @ForeignCollectionField(columnName = "roba",eager = false)
    private ForeignCollection<Roba> SpisakRobe;

    @Override
    public String toString() {return "ID: " + id + " Opis: " + opis + " Raspon krila: " + raspon_krila;}

    public Avion()
    {

    }
    public Avion(String opis,int raspon_krila)
    {
        this.opis = opis;
        this.raspon_krila = raspon_krila;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public int getRaspon_krila() {
        return raspon_krila;
    }

    public void setRaspon_krila(int raspon_krila) {
        this.raspon_krila = raspon_krila;
    }


}
