package model;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;

import java.util.ArrayList;
import java.util.List;

public class AvionNit extends Thread {

    private Avion A;
    public static boolean DozvoljenoPoletanje = true;
    public static Object O = new Object();

    public void run()
    {
        System.out.println( "Pocinju provere za avion: " + A.getId() );

        try
        {
            Thread.sleep( (int)(Math.random() * 2));
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        System.out.println("Avion " + A.getId() + " je spreman za poletanje, i ceka dozvolu...");


        boolean permit = false;

        do {
            synchronized (O) {

                if (DozvoljenoPoletanje) {
                    DozvoljenoPoletanje = false;
                    permit = true;
                }
                else
                    permit = false;
            }
            System.out.println("Avion" + A.getId() + "proverava permit..");
            if (!permit)
            {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        while(!permit);

        // POLECE
        System.out.println("Avion " + A.getId() + "izlazi na pistu i polece");

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        synchronized (O) {
            System.out.println("Avion " + A.getId() + "je poleteo!");
            DozvoljenoPoletanje = true;
            System.out.println("Avion " + A.getId() + "je na kraju poletanja..");
        }

    }

    public static void main(String[] args) {
        ConnectionSource cs = null;
        try
        {

            cs = new JdbcConnectionSource("jdbc:sqlite:avionRoba.db");
            Dao<Avion,Integer> AvionDao = DaoManager.createDao(cs, Avion.class);
            List<AvionNit> NitiAviona = new ArrayList<>();

            List<Avion> Avioni = AvionDao.queryForAll();

            for (Avion A: Avioni) {
                AvionNit AN = new AvionNit();
                AN.setA(A);
                NitiAviona.add(AN);

                // Startujemo datu nit
                AN.start();
            }

            // Joinujemo svve niti..
            for (AvionNit Nit : NitiAviona) {
                Nit.join();
            }
            System.out.println("Svi avioni su poleteli!");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }


    public Avion getA() {
        return A;
    }

    public void setA(Avion a) {
        A = a;
    }
}
